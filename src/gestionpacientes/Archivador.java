/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionpacientes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author nacho
 */
public class Archivador {
    private LinkedList<Ficha> fichas=new LinkedList();
    
    public void añadir(String nombre, String apellido, int edad) {
        Ficha f= new Ficha(nombre, apellido, edad);
        fichas.add(f);
    }
    
    public Ficha obtener(int id) {
        Ficha resultado=null;
        Ficha f;
        Iterator it=fichas.iterator();
        while (it.hasNext()) {
            f=(Ficha)it.next();
            if (f.getIdPaciente()==id) {
                resultado=f;
                break;
            }
        }
        return resultado;
    }
    
    public void borrar(int id) {
        Ficha f;
        Iterator it= fichas.iterator();
        while (it.hasNext()) {
            f=(Ficha)it.next();
            if (f.getIdPaciente()==id) {
                fichas.remove(f);
                break;
            }
        }
    }
    
    public String listado() {
        Ficha f;
        String listado= "Listado de Pacientes\n";
        listado+=       "--------------------\n";
        Iterator it= fichas.iterator();
        while (it.hasNext()) {
            f=(Ficha)it.next();
            listado+=f.getIdPaciente() + " - " + f.toString() + "\n";
        }
        listado+="\nNúm Fichas: " + fichas.size() + "\n";
        return listado;
    }
}
