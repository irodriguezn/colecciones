/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionpacientes;

//import java.util.Scanner;
import utilidades.Utilidades;

/**
 *
 * @author nacho
 */
public class GestionPacientes {
    public static void main(String[] args) {
        int op;
        Archivador a1= new Archivador();
        do {
            op=menuCrud("Mantenimiento Pacientes", "Paciente");
            gestionarCrud(op, a1);
        } while (op!=0);
    }
    
    static int menuCrud(String titulo, String subtitulo) {
        int op=0;
        boolean salir=false;
        System.out.println("");
        System.out.println(titulo);
        System.out.println("");
        System.out.println("1- Introducir " + subtitulo);
        System.out.println("2- Listado " + subtitulo + "s");
        System.out.println("3- Actualizar " + subtitulo);
        System.out.println("4- Borrar " + subtitulo);
        System.out.println("0- Salir");
        System.out.println("");
        op=Utilidades.validarEntero("Introduzca Opción (0-4): ", 0, 4);
        return op;
    }
    
    static void gestionarCrud(int op, Archivador a) {
        System.out.println("");
        switch (op) {
            case 0: 
                break;
            case 1:
                crearPaciente(a);
                break;
            case 2:
                leerPaciente(a);
                break;
            case 3:
                actualizarPaciente(a);
                break;
            case 4:
                borrarPaciente(a);
                break;
        }
    }
    
    static void crearPaciente(Archivador a) {
        String nombre=Utilidades.validarString("Nombre del Paciente: ", 25);
        String apellidos=Utilidades.validarString("Apellidos del Paciente: ", 50);
        int edad=Utilidades.validarEntero("Edad del Paciente: ", 0, 175);
        a.añadir(nombre, apellidos, edad);
        System.out.println("");
        System.out.println(a.listado());
    }
    
    static void actualizarPaciente(Archivador a) {
        System.out.println(a.listado());
        boolean salir=false;
        int idPaciente=0;
        idPaciente=Utilidades.validarEntero("IdPaciente: ");
        Ficha f=a.obtener(idPaciente);
        if (f!=null) {
            int op;
            Ficha fc=(Ficha)f.Clone();
            do {
                op=menuActualizar(fc);
                gestionarActualizar(op, fc);
            } while (op!=0 && op!=4);
            if (op==4) {
                f.setApellido(fc.getApellido());
                f.setNombre(fc.getNombre());
                f.setEdad(fc.getEdad());
                //a.actualizar(fc);
            }
        } else {
            System.out.println("\nLa ficha no existe");
        }
    }
    
    static int menuActualizar(Ficha f) {
        System.out.println("");
        System.out.println("Paciente "+f.getIdPaciente());
        System.out.println("------------");
        System.out.println("1- Nombre: " + f.getNombre());
        System.out.println("2- Apellidos: " + f.getApellido());
        System.out.println("3- Edad: " + f.getEdad());
        System.out.println("4- Guardar");
        System.out.println("0- Cancelar");
        System.out.println("");
        int op=Utilidades.validarEntero("Introduzca Campo a Modificar (0-4): ",0 ,4);
        return op;
    }

    static void gestionarActualizar(int op, Ficha f) {
        System.out.println("");
        //Scanner sc = new Scanner(System.in);
        switch (op) {
            case 0: 
                break;
            case 1: // Actualizar nombre
                String nombre=Utilidades.validarString("Nuevo nombre: ", 50);
                f.setNombre(nombre);
                break;
            case 2:
                String apellido=Utilidades.validarString("Nuevo apellido: ", 50);
                f.setApellido(apellido);
                break;
            case 3:
                int edad=Utilidades.validarEntero("Nueva edad: ");
                f.setEdad(edad);
                break;
        }
    }

    static void leerPaciente(Archivador a) {
        System.out.println(a.listado());
    }
    
    static void borrarPaciente(Archivador a) {
        System.out.println(a.listado());
        System.out.println("");
        int idPaciente=Utilidades.validarEntero("IdPaciente: ");
        a.borrar(idPaciente);
    }
    

    
}
